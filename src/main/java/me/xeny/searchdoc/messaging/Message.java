package me.xeny.searchdoc.messaging;

import java.io.Serializable;
import java.util.UUID;

public abstract class Message<P> implements Serializable {
    private UUID id;
    private P payload;

    public Message(UUID id, P payload) {
        this.id = id;
        this.payload = payload;
    }

    public Message(P payload) {
        this.id = UUID.randomUUID();
        this.payload = payload;
    }

    public P getPayload() {
        return payload;
    }

    public abstract String getTopic();
}
