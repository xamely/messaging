package me.xeny.searchdoc.messaging.impl;

import java.io.Serializable;

public class PrintPayload implements Serializable {
    private String text;

    public PrintPayload(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
