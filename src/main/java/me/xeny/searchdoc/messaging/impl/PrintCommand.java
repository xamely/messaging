package me.xeny.searchdoc.messaging.impl;

import me.xeny.searchdoc.messaging.Command;

import java.util.UUID;

public class PrintCommand extends Command<PrintPayload> {

    public PrintCommand(UUID id, PrintPayload payload) {
        super(id, payload);
    }

    public PrintCommand(PrintPayload payload) {
        super(payload);
    }

    @Override
    public String getTopic() {
        return Topic.NAME;
    }

    public static class Topic {
        public static final String NAME = "print_topic";
    }
}
