package me.xeny.searchdoc.messaging;

import java.util.UUID;

public abstract class Event<P> extends Message {
    public Event(UUID id, Object payload) {
        super(id, payload);
    }

    public Event(Object payload) {
        super(payload);
    }
}
