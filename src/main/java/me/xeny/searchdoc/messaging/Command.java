package me.xeny.searchdoc.messaging;

import java.util.UUID;

public abstract class Command<P> extends Message<P> {
    public Command(UUID id, P payload) {
        super(id, payload);
    }

    public Command(P payload) {
        super(payload);
    }
}
